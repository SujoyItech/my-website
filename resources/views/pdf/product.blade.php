<!DOCTYPE html>
<html>
<head>
    <title>Hi</title>
    <style>
        .header{
            column-count: 3;
            column-gap: 40px;
        }
        .logo,.address,.contact{
            width: 300px;
            height: 100px;
        }
        .middle h2{
            text-align: center;
        }

    </style>
</head>
<body>
    <div class="header">
        <div class="logo">
            <img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" style="width: 200px;">
        </div>
        <div class="contact">
            <address>
                www.brinde-company.pt<br>
                Email:info@brinde-company.pt<br>
                Nif:504803234<br>
                Tel:+352346645444 | Fax:+351346664<br>
            </address>
        </div>
        <div class="address">
            <address>
                Written by <a href="mailto:webmaster@example.com">Jon Doe</a>.<br>
                Visit us at:<br>
                Example.com<br>
                Box 564, Disneyland<br>
                USA
            </address>
        </div>
    </div>
    <div class="middle">
        <hr>
        <h2>Welcome to ItSolutionStuff.com - {{ $title ?? '' }}</h2>
        <hr>
        <div class="order">
            <h2>Title</h2>
            <h3>BC-AM-53327-202101-0189</h3>
        </div>
        <div class="date">
            <h2>Title</h2>
            <h3>BC-AM-53327-202101-0189</h3>
        </div>
        <div class="user">
            <h2>Title</h2>
            <h3>User Name</h3>
        </div>
    </div>

    <div class="middle">
        <textarea rows="3">Client: Paulo Gouveia</textarea>
    </div>

    <div class="product-section">
        <table>
            <tr>
                <th>Size</th>
                <th>Description</th>
                <th>INF.Extra</th>
                <th>QTD</th>
                <th>Preco uni</th>
                <th>IVA</th>
            </tr>
            <tr>
                <td><img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" style="width: 200px;"></td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua</td>
                <td>Extra</td>
                <td>2</td>
                <td>39000</td>
                <td>23.00%</td>
                <td>Sem iva</td>
            </tr>
            <tr>
                <td><img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" style="width: 200px;"></td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua</td>
                <td>Extra</td>
                <td>2</td>
                <td>39000</td>
                <td>23.00%</td>
                <td>Sem iva</td>
            </tr>
            <tr>
                <td><img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" style="width: 200px;"></td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua</td>
                <td>Extra</td>
                <td>2</td>
                <td>39000</td>
                <td>23.00%</td>
                <td>Sem iva</td>
            </tr>
            <tr>
                <td><img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" style="width: 200px;"></td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua</td>
                <td>Extra</td>
                <td>2</td>
                <td>39000</td>
                <td>23.00%</td>
                <td>Sem iva</td>
            </tr>
            <tr>
                <td><img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" style="width: 200px;"></td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua</td>
                <td>Extra</td>
                <td>2</td>
                <td>39000</td>
                <td>23.00%</td>
                <td>Sem iva</td>
            </tr>
            <tr>
                <td><img src="https://image.shutterstock.com/image-photo/bright-spring-view-cameo-island-260nw-1048185397.jpg" style="width: 200px;"></td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua</td>
                <td>Extra</td>
                <td>2</td>
                <td>39000</td>
                <td>23.00%</td>
                <td>Sem iva</td>
            </tr>

        </table>
    </div>

    </body>
</html>




