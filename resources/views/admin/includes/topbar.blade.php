<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">
            {{--Web View Search--}}
            <li class="d-none d-lg-block">
                <form class="app-search">
                    <div class="app-search-box">
                        <div class="input-group">
                            <input type="search" class="form-control" placeholder="Search...">
                            <div class="input-group-append">
                                <button class="btn" type="submit">
                                    <i class="fe-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </li>
            {{--Mobile View Search--}}
            <li class="dropdown d-inline-block d-lg-none">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="fe-search noti-icon"></i>
                </a>
                <div class="dropdown-menu dropdown-lg dropdown-menu-right p-0">
                    <form class="p-3">
                        <input type="text" class="form-control" placeholder="Search ..." aria-label="Search Over Site">
                    </form>
                </div>
            </li>
            {{--Window Maximize--}}
            <li class="dropdown d-none d-lg-inline-block">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">
                    <i class="fe-maximize noti-icon"></i>
                </a>
            </li>
            {{--Theme Switch--}}
            {{--<li class="d-none d-lg-block py-3">
                <input class="" type="checkbox" checked data-plugin="switchery" data-color="#2b3d51" data-size="small"/>
            </li>--}}
            {{--Localization--}}
            <li class="dropdown d-none d-lg-inline-block topbar-dropdown">
                @include('admin.includes.languages')
            </li>
            {{--Notification--}}
            <li class="dropdown notification-list topbar-dropdown">
                @include('admin.includes.notifications')
            </li>

            <li class="dropdown notification-list">
                <a href="javascript:void(0);" class="nav-link right-bar-toggle nav-user">
                    <img src="{{adminAsset('images/users/user-9.jpg')}}" alt="user-image" class="rounded-circle">
                    <span class="pro-user-name ml-1"> {{\Illuminate\Support\Facades\Auth::user()->name ?? ''}} <i class="mdi mdi-chevron-down"></i> </span>
                </a>
            </li>
        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="{{url('/')}}" class="logo logo-dark text-center">
                <span class="logo-sm"><img src="{{isset($settings->app_logo_small) && !empty($settings->app_logo_small) ? asset(get_image_path('settings').'/'.$settings->app_logo_small) : adminAsset('images/logo-sm.png') }}" alt="" height="22"></span>
                <span class="logo-lg"><img src="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? asset(get_image_path('settings').'/'.$settings->app_logo_large) : adminAsset('images/logo-sm.png') }}" alt="" height="20"></span>
            </a>
            <a href="{{url('/')}}" class="logo logo-light text-center">
                <span class="logo-sm"><img src="{{isset($settings->app_logo_small) && !empty($settings->app_logo_small) ? asset(get_image_path('settings').'/'.$settings->app_logo_small) : adminAsset('images/logo-sm.png') }}" alt="" height="22"></span>
                <span class="logo-lg"><img src="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? asset(get_image_path('settings').'/'.$settings->app_logo_large) : adminAsset('images/logo-sm.png') }}" alt="" height="20"></span>
            </a>
        </div>
        {{--Menu Bar Icon--}}
        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>
            <li>
                <!-- Mobile menu toggle (Horizontal Layout)-->
                <a class="navbar-toggle nav-link" data-toggle="collapse" data-target="#topnav-menu-content">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>

        </ul>
        <div class="clearfix"></div>
    </div>
</div>
