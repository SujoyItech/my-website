<h5 class="menu-title"><i class="fas fa-boxes"></i> {{__('Personal')}}</h5>
<ul class="nav flex-column">
    <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'brands') menuitem-active @endif">
        <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'brands') active @endif" href="{{route('brands')}}"><i class="fas fa-store"></i> {{__('Brands')}}</a>
    </li>
    <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'education') menuitem-active @endif">
        <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'education') active @endif" href="{{route('education')}}"><i class="fas fa-school"></i> {{__('Education')}}</a>
    </li>
    <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'skill') menuitem-active @endif">
        <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'skill') active @endif" href="{{route('skill')}}"><i class="fas fa-school"></i> {{__('Skills')}}</a>
    </li>
</ul>
