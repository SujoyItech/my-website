<h5 class="menu-title"><i class="fa fa-cog"></i> {{__('Settings')}}</h5>
<ul class="nav flex-column">
    <li class="nav-item">
        <a class="nav-link" href="#"><i class="fa fa-user"></i> {{__('Profile Settings')}}</a>
    </li>
    <li class="nav-item">
        <a href="#general-settings @if(!empty($sub_menu) && in_array($sub_menu,['siteSettings','logoSettings','socialSettings'])) show @endif" data-toggle="collapse" class="nav-link">
            <span><i class="fa fa-cog"></i>  {{__('General Settings')}} </span>
            <span class="menu-arrow"></span>
        </a>
        <div class="collapse" id="general-settings">
            <ul class="nav-second-level">
                @if(check_route_permission('siteSetting'))
                <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'siteSettings') menuitem-active @endif">
                    <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'siteSettings') active @endif" href="{{route('siteSetting')}}"><i class="fa fa-globe"></i> {{__('Site Settings')}}</a>
                </li>
                @endif
                @if(check_route_permission('logoSetting'))
                <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'logoSettings') menuitem-active @endif">
                    <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'logoSettings') active @endif" href="{{route('logoSetting')}}"><i class="fab fa-react"></i> {{__('Logo Manager')}}</a>
                </li>
                @endif
                @if(check_route_permission('socialSetting'))
                <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'socialSettings') menuitem-active @endif">
                    <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'socialSettings') active @endif" href="{{route('socialSetting')}}"><i class="fa fa-link"></i> {{__('Social Links')}}</a>
                </li>
                @endif
            </ul>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"><i class="fa fa-envelope"></i> {{__('Email Settings')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"><i class="fa fa-paper-plane"></i> {{__('SMS Settings')}}</a>
    </li>
    <li class="nav-item">
        <a href="#web-contents" data-toggle="collapse" class="nav-link">
            <span><i class="fa fa-file"></i>  {{__('Web Contents')}} </span>
            <span class="menu-arrow"></span>
        </a>
        <div class="collapse" id="web-contents">
            <ul class="nav-second-level">
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-file"></i> {{__('Terms & Condition')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-file"></i> {{__('Privacy Policy')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-file"></i> {{__('FAQs')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-file"></i> {{__('About Us')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-file"></i> {{__('Why Choose Us')}}</a>
                </li>
            </ul>
        </div>
    </li>
</ul>
