<div class="right-bar">
    <div data-simplebar class="h-100">
        <div class="p-3">
            <h4 class="">{{\Illuminate\Support\Facades\Auth::user()->name}}</h4>
            <h5 class="">{{\Illuminate\Support\Facades\Auth::user()->email}}</h5>
            <hr/>
            <h5><a href="{{ route('profile') }}" class="text-dark"> <i class="fe-user"></i> {{__('Profile Settings')}} </a></h5>
            <h5><a href="javascript:void(0);" class="text-dark"> <i class="fe-user"></i> My Orders </a></h5>
            <h5><a href="javascript:void(0);" class="text-dark"> <i class="fe-user"></i> My Orders </a></h5>
            <h5><a href="javascript:void(0);" class="text-dark"> <i class="fe-user"></i> Password Change </a></h5>
            {{--Theme Color--}}
{{--            <h6 class="font-weight-medium font-14 mt-4 mb-2 pb-1">{{__('Theme Color')}}</h6>--}}
{{--            <div class="custom-control custom-switch mb-1">--}}
{{--                <input type="radio" class="custom-control-input" name="color-scheme-mode" value="light"--}}
{{--                       id="light-mode-check" checked />--}}
{{--                <label class="custom-control-label" for="light-mode-check">Light Mode</label>--}}
{{--            </div>--}}

{{--            <div class="custom-control custom-switch mb-1">--}}
{{--                <input type="radio" class="custom-control-input" name="color-scheme-mode" value="dark"--}}
{{--                       id="dark-mode-check" />--}}
{{--                <label class="custom-control-label" for="dark-mode-check">Dark Mode</label>--}}
{{--            </div>--}}

{{--            <!-- Topbar -->--}}
{{--            <h6 class="font-weight-medium font-14 mt-4 mb-2 pb-1">Topbar</h6>--}}

{{--            <div class="custom-control custom-switch mb-1">--}}
{{--                <input type="radio" class="custom-control-input" name="topbar-color" value="dark" id="darktopbar-check"--}}
{{--                       checked />--}}
{{--                <label class="custom-control-label" for="darktopbar-check">Dark</label>--}}
{{--            </div>--}}

{{--            <div class="custom-control custom-switch mb-1">--}}
{{--                <input type="radio" class="custom-control-input" name="topbar-color" value="light" id="lighttopbar-check" />--}}
{{--                <label class="custom-control-label" for="lighttopbar-check">Light</label>--}}
{{--            </div>--}}

{{--            <!-- Left Sidebar-->--}}
{{--            <h6 class="font-weight-medium font-14 mt-4 mb-2 pb-1">Left Sidebar Color</h6>--}}

{{--            <div class="custom-control custom-switch mb-1">--}}
{{--                <input type="radio" class="custom-control-input" name="leftsidebar-color" value="light" id="light-check" checked />--}}
{{--                <label class="custom-control-label" for="light-check">Light</label>--}}
{{--            </div>--}}

{{--            <div class="custom-control custom-switch mb-1">--}}
{{--                <input type="radio" class="custom-control-input" name="leftsidebar-color" value="dark" id="dark-check" />--}}
{{--                <label class="custom-control-label" for="dark-check">Dark</label>--}}
{{--            </div>--}}

{{--            <div class="custom-control custom-switch mb-1">--}}
{{--                <input type="radio" class="custom-control-input" name="leftsidebar-color" value="brand" id="brand-check" />--}}
{{--                <label class="custom-control-label" for="brand-check">Brand</label>--}}
{{--            </div>--}}

{{--            <div class="custom-control custom-switch mb-3">--}}
{{--                <input type="radio" class="custom-control-input" name="leftsidebar-color" value="gradient" id="gradient-check" />--}}
{{--                <label class="custom-control-label" for="gradient-check">Gradient</label>--}}
{{--            </div>--}}
            <hr/>
            <h5>
                <div class="p-2 border-t border-theme-40 dark:border-dark-3">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a href="{{ route('logout') }}" class="text-danger"  onclick="event.preventDefault(); this.closest('form').submit();"> <i class="fa fa-sign-out-alt"></i> Logout </a>
                    </form>
                </div>
            </h5>
        </div>
    </div> <!-- end slimscroll-menu-->
</div>

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
