<div class="left-side-menu">
    <div class="h-100" >
        <div class="sidebar-content">
            <div class="sidebar-icon-menu h-100" data-simplebar>
                <!-- LOGO -->
                <a href="{{url('/')}}" class="logo">
                    <span>
                        <img src="{{isset($settings->app_logo_small) && !empty($settings->app_logo_small) ?  asset(get_image_path('settings').'/'.$settings->app_logo_small) : adminAsset('images/logo-sm.png')}}" alt="" height="28">
                    </span>
                </a>
                <nav class="nav flex-column" id="two-col-sidenav-main">
                    <a class="nav-link @if(!empty($menu) && $menu == 'home') active @endif"  href="#dashboard"
                       data-plugin="tippy"
                       data-tippy-followCursor="true"
                       data-tippy-arrow="true"
                       data-tippy-placement=""
                       data-tippy-animation="fade"
                       data-tippy-theme="gradient"
                       title="Dashboard">
                        <i class="fa fa-home fa-2x"></i>
                    </a>
                    <a class="nav-link @if(!empty($menu) && $menu == 'product') active @endif" href="#product-management"
                       data-plugin="tippy"
                       data-tippy-followCursor="true"
                       data-tippy-arrow="true"
                       data-placement="right"
                       data-tippy-animation="fade"
                       title="Product Management"><i class="fas fa-boxes fa-2x"></i></a>

                    <a class="nav-link @if(!empty($menu) && $menu == 'settings') active @endif" href="#settings"
                       data-plugin="tippy"
                       data-tippy-followCursor="true"
                       data-tippy-arrow="true"
                       data-placement="right"
                       data-tippy-animation="fade"
                       title="Settings" data-trigger="hover"><i class="fa fa-cog fa-2x"></i></a>
                </nav>
            </div>
            <!--- Sidemenu -->
            <div class="sidebar-main-menu">
                <div id="two-col-menu" class="h-100" data-simplebar>
                    <div class="twocolumn-menu-item d-block" id="dashboard">
                        <div class="title-box">
                            <h5 class="menu-title">{{__('Dashboards')}}</h5>
                            <ul class="nav flex-column">
                                <li class="nav-item @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif">
                                    <a class="nav-link @if(!empty($sub_menu) && $sub_menu == 'dashboard') menuitem-active @endif" href="{{route('adminHome')}}">{{__('Dashboard')}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'settings') d-block @endif" id="settings">
                        @include('admin.includes.left_menus.settings')
                    </div>
                    <div class="twocolumn-menu-item @if(!empty($menu) && $menu == 'product') d-block @endif" id="product-management">
                        @include('admin.includes.left_menus.product_management')
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
