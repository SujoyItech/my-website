<div class="card-box text-center">
    <img src="{{asset(get_image_path('user').'/'.\Illuminate\Support\Facades\Auth::user()->profile_photo_path)}}" class="rounded-circle avatar-lg img-thumbnail"
         alt="profile-image">

    <h4 class="mb-0">{{$profile->name ?? ''}}</h4>

    <div class="text-left mt-3">
        <h4 class="font-13 text-uppercase">{{__('About Me :')}}</h4>
        <p class="text-muted mb-2 font-13">
            <strong>{{__('Name :')}}</strong>
            <span class="ml-2">{{$profile->name ?? ''}}</span>
        </p>

        <p class="text-muted mb-2 font-13"><strong>{{__('Email :')}}</strong> <span class="ml-2 ">{{$profile->email ?? ''}}</span></p>
        <p class="text-muted mb-2 font-13"><strong>{{__('Type :')}}</strong> <span class="ml-2 ">{{module_name($profile->module_id)}}</span></p>
    </div>
</div>
