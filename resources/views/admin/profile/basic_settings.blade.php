<div class="card-box">
    <form action="{{route('updateProfile')}}" class="parsley-examples" method="POST" id="user_profile_update" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="name">{{__('Name')}}</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter full name" value="{{$profile->name ?? ''}}" required >
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label for="email">{{__('Email')}}</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$profile->email ?? ''}}" required >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-form-label" for="profile_photo_path">{{__('Picture')}}</label>
                    <input type="file" name="profile_photo_path" data-plugins="dropify" class="dropify"
                           data-default-file="{{isset($profile->profile_photo_path) ? asset(get_image_path('user').'/'.$profile->profile_photo_path) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="2M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                </div>
            </div>
        </div>

        <div class="text-left">
            <input type="hidden" name="id" value="{{$profile->id ?? ''}}">
            <button type="submit" class="btn btn-success waves-effect waves-light mt-2 submit_basic"><i class="fa fa-save"></i> {{__('Save')}}</button>
        </div>
    </form>
</div>
