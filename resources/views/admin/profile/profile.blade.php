@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'profileSettings','title'=>__('Profile settings')])
@section('content')
    <div class="col-lg-4 col-xl-4">
        @include('admin.profile.profile_card')
    </div>
    <div class="col-lg-8 col-xl-8">
        @include('admin.profile.basic_settings')
        @include('admin.profile.password_settings')
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            submitOperation(submitResponse, 'submit_basic');
            submitOperation(submitPasswordResponse, 'submit_password');
            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                } else {
                    swalError(response.message);
                }
            }
            function submitPasswordResponse(response, this_form){
                if (response.success == true) {
                    $('form :input').val('');
                    this_form.removeClass('was-validated')
                    swalSuccess(response.message);
                } else {
                    swalError(response.message);
                }
            }
        });
    </script>
@endsection
