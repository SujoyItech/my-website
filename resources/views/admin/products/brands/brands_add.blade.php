<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Brand  Add/Edit')}}</h4>
        <form class="brands-form" novalidate method="post" action="{{route('saveBrand')}}" id="brand_form">
            <div class="form-group mb-2">
                <label for="name">{{__('Name')}}</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('Name')}}" value="{{$brand->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <label for="slug">{{__('Slug')}}<span class="text-danger">*</span></label>
                <input type="text" class="form-control check_slug_validity" data-slugforid="name" data-exceptvalueid="id" data-slugvalidateurl="{{route('brandSlugCheck')}}" name="slug" id="slug" value="{{$brand->slug ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="store_url">{{__('Brand url')}}</label>
                <input type="text" class="form-control" name="brand_url" id="brand_url" placeholder="{{__('Url')}}" value="{{$brand->brand_url ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label class="col-form-label" for="icon">{{__('Icon')}}</label>
                <input type="file" name="icon" id="icon" class="dropify"
                       data-default-file="{{isset($brand->icon) ? asset(get_image_path('brand').'/'.$brand->icon) : ''}}"/>
            </div>

            <div class="form-group mb-2">
                <label>{{__('Status')}}</label>
                <select class="form-control" name="status" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="{{STATUS_ACTIVE}}" {{is_selected(STATUS_ACTIVE,$brand->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{STATUS_PROCESSING}}" {{is_selected(STATUS_PROCESSING,$brand->status ?? '')}}>{{__('Inactive')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="checkbox checkbox-success mb-2">
                    <input id="isReseller" type="checkbox" name="is_reseller" {{isset($brand->is_reseller) && $brand->is_reseller == TRUE ? 'checked' : ''}}>
                    <label for="isReseller">
                        {{__('Is reseller')}}
                    </label>
                </div>
            </div>

            <input type="hidden" name="id" value="{{$brand->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-info waves-effect waves-light reset_from" type="button" onclick="reset_form(true,function (){ $('input:checkbox').removeAttr('checked') })"><i class="fa fa-refresh"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('brands-form');
        checkSlugVlaidity('user');
    });
</script>
