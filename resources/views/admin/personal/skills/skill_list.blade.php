<div class="card-box">
    <h4 class="header-title">{{__('Skill List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the skill list')}}
    </p>

    <div class="table-responsive">
        <table id="skill_table" class="table dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th>{{__('Icon')}}</th>
                <th>{{__('Title')}}</th>
                <th>{{__('Level')}}</th>
                <th>{{__('Experience Type')}}</th>
                <th>{{__('Experience Year')}}</th>
                <th>{{__('Total Project')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
