<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Skill  Add/Edit')}}</h4>
        <form class="skill-form" novalidate method="post" action="{{route('saveEducation')}}" id="skill_form_id">
            <div class="form-group mb-2">
                <label for="title">{{__('Title')}}</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="{{__('Laravel/Django/Mysql')}}" value="{{$skill->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="degree">{{__('Category')}}</label>
                <select class="form-control" name="experience_category">
                    <option value="">{{__('Select')}}</option>
                    <option value="{{FRONT_END}}">{{__('FrontEnd')}}</option>
                    <option value="{{BACK_END}}">{{__('Backend')}}</option>
                    <option value="{{DATABASE}}">{{__('Database')}}</option>
                    <option value="{{DEVOPS}}">{{__('Devops')}}</option>
                    <option value="{{OTHERS}}">{{__('Others')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="degree">{{__('Level')}}</label>
                <select class="form-control" name="level">
                    <option value="">{{__('Select')}}</option>
                    <option value="{{BEGINNERS}}">{{__('Beginner')}}</option>
                    <option value="{{AVERAGE}}">{{__('Average')}}</option>
                    <option value="{{SKILLED}}">{{__('Skilled')}}</option>
                    <option value="{{SPECIALIST}}">{{__('Specialist')}}</option>
                    <option value="{{EXPERT}}">{{__('Expert')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="degree">{{__('Learned From')}}</label>
                <select class="form-control" name="learned_from">
                    <option value="">{{__('Select')}}</option>
                    <option value="{{PERSONAL_EXPERIENCE}}">{{__('Personal Experience')}}</option>
                    <option value="{{COMPANY_EXPERIENCE}}">{{__('Company Experience')}}</option>
                    <option value="{{ONLINE_COURSE}}">{{__('Online Course')}}</option>
                    <option value="{{OTHERS}}">{{__('Others')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="experience_year">{{__('Experience year')}}</label>
                <input type="number" class="form-control" name="experience_year" id="experience_year" step="0.01" placeholder="{{__('Ex.0.5-1.0')}}" value="{{$skill->experience_year ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="total_project">{{__('Total project')}}</label>
                <input type="number" class="form-control" name="total_project" id="total_project"  placeholder="" value="{{$skill->total_project ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="total_project">{{__('Company project')}}</label>
                <input type="number" class="form-control" name="industrial_project" id="industrial_project"  placeholder="" value="{{$skill->industrial_project ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="total_project">{{__('Personal project')}}</label>
                <input type="number" class="form-control" name="personal_project" id="personal_project"  placeholder="" value="{{$skill->personal_project ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <div class="form-group mb-2">
                <label for="description">{{__('Description')}}</label>
                <textarea type="text" class="form-control" name="description" placeholder="{{__('Some description here')}}">{{$skill->description ?? ''}}</textarea>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="tags">{{__('Tags')}}</label>
                <input type="text" value="{{$skill->tags ?? ''}}" data-role="tagsinput" name="tags[]">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <div class="form-group mb-2">
                <label class="col-form-label" for="icon">{{__('Skill Icon')}}</label>
                <input type="file" name="icon" id="icon" class="dropify"
                       data-default-file="{{isset($skill->icon) ? asset(get_image_path('skill').'/'.$skill->icon) : ''}}"/>
            </div>
            <div class="form-group mb-2">
                <label>{{__('Status')}}</label>
                <select class="form-control" name="status" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="{{STATUS_ACTIVE}}" {{is_selected(STATUS_ACTIVE,$skill->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{STATUS_PROCESSING}}" {{is_selected(STATUS_PROCESSING,$skill->status ?? '')}}>{{__('Inactive')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <input type="hidden" name="id" value="{{$skill->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-info waves-effect waves-light reset_from" type="button" onclick="reset_form(true)"><i class="fa fa-refresh"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('skill-form');
    });
</script>
