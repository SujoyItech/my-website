@extends('admin.layouts.app',['menu'=>'personal','sub_menu'=>'skill'])
@section('style')
    <link href="{{adminAsset('multiselect/tagsinput.css')}}" rel="stylesheet" type="text/css">
    <script src="{{adminAsset('multiselect/tagsinput.js')}}"></script>
@endsection
@section('content')
    <div class="col-4 add_edit">
        @include('admin.personal.skills.skill_add')
    </div>
    <div class="col-8">
        @include('admin.personal.skills.skill_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('skills')}}";
            var delete_url = "{{route('deleteSkill')}}";
            var data_column =  [
                {"data": "icon",orderable: false, searchable: false},
                {"data": "title"},
                {"data": "experience_category"},
                {"data": "level"},
                {"data": "learned_from"},
                {"data": "experience_year"},
                {"data": "total_project"},
                {"data": "industrial_project"},
                {"data": "personal_project"},
                {"data": "icon"},
                {"data": "description"},
                {"data": "tags"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#skill_table'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);
            editOperation(editResponse,"{{route('editSkill')}}",true);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    clearDropify();
                    this_form.removeClass('was-validated')
                    renderDataTable($('#skill_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                $('.dropify').dropify();
                submitOperation(submitResponse, 'submit_basic');
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#skill_table'),data_url,data_column);
                }
            }
        });
    </script>
@endsection

