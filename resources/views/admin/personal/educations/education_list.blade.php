<div class="card-box">
    <h4 class="header-title">{{__('Education List')}}</h4>
    <p class="sub-header">
        {{__('Here goes the education list')}}
    </p>

    <div class="table-responsive">
        <table id="education_table" class="table dt-responsive nowrap w-100">
            <thead>
            <tr>
                <th>{{__('Icon')}}</th>
                <th>{{__('Title')}}</th>
                <th>{{__('Group/Degree')}}</th>
                <th>{{__('Institution')}}</th>
                <th>{{__('Academic year')}}</th>
                <th>{{__('Obtained GPA/CGPA')}}</th>
                <th>{{__('Total GPA/CGPA')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
