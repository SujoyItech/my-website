<div class="card">
    <div class="card-body ajax-load">
        <h4 class="card-title">{{__('Education  Add/Edit')}}</h4>
        <form class="education-form" novalidate method="post" action="{{route('saveEducation')}}" id="education_form_id">
            <div class="form-group mb-2">
                <label for="name">{{__('Title')}}</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('SSC/HSC/University')}}" value="{{$education->name ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="degree">{{__('Group/Degree')}}</label>
                <input type="text" class="form-control" name="degree" id="degree" placeholder="{{__('Group/Degree')}}" value="{{$education->degree ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="institution">{{__('Institution name')}}</label>
                <input type="text" class="form-control" name="institution" id="institution" placeholder="{{__('School/College/University')}}" value="{{$education->institution ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="academic_year">{{__('Academic year')}}</label>
                <input type="text" class="form-control" name="academic_year" id="academic_year" placeholder="{{__('Ex.2014-2019')}}" value="{{$education->academic_year ?? ''}}" required>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="obtain_result">{{__('GPA/CGPA')}}</label>
                <input type="number" class="form-control" name="obtain_result" step="0.01" id="obtain_result" placeholder="{{__('5.0')}}" value="{{$education->obtain_result ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label for="out_of_result">{{__('Out of GPA/CGPA')}}</label>
                <input type="number" class="form-control" name="out_of_result" step="0.01" id="out_of_result" placeholder="{{__('5.0')}}" value="{{$education->out_of_result ?? ''}}">
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>
            <div class="form-group mb-2">
                <label class="col-form-label" for="icon">{{__('Institution Icon')}}</label>
                <input type="file" name="icon" id="icon" class="dropify"
                       data-default-file="{{isset($education->icon) ? asset(get_image_path('education').'/'.$education->icon) : ''}}"/>
            </div>
            <div class="form-group mb-2">
                <label class="col-form-label" for="icon">{{__('Certificate')}}</label>
                <input type="file" name="certificate" id="certificate" class="dropify"
                       data-default-file="{{isset($education->certificate) ? asset(get_image_path('education').'/'.$education->certificate) : ''}}"/>
            </div>
            <div class="form-group mb-2">
                <label>{{__('Status')}}</label>
                <select class="form-control" name="status" required>
                    <option value="">{{__('Select')}}</option>
                    <option value="{{STATUS_ACTIVE}}" {{is_selected(STATUS_ACTIVE,$education->status ?? '')}}>{{__('Active')}}</option>
                    <option value="{{STATUS_PROCESSING}}" {{is_selected(STATUS_PROCESSING,$education->status ?? '')}}>{{__('Inactive')}}</option>
                </select>
                <div class="valid-feedback">
                    {{__('Looks good!')}}
                </div>
            </div>

            <input type="hidden" name="id" value="{{$education->id ?? ''}}">
            <button class="btn btn-dark waves-effect waves-light submit_basic" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            <button class="btn btn-info waves-effect waves-light reset_from" type="button" onclick="reset_form(true)"><i class="fa fa-refresh"></i> {{__('Reset')}}</button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function (){
        resetValidation('education-form');
    });
</script>
