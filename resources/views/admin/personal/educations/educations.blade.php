@extends('admin.layouts.app',['menu'=>'personal','sub_menu'=>'education'])
@section('content')
    <div class="col-4 add_edit">
        @include('admin.personal.educations.education_add')
    </div>
    <div class="col-8">
        @include('admin.personal.educations.education_list')
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
            var data_url = "{{route('education')}}";
            var delete_url = "{{route('deleteEducation')}}";
            var data_column =  [
                {"data": "icon",orderable: false, searchable: false},
                {"data": "name"},
                {"data": "degree"},
                {"data": "institution"},
                {"data": "academic_year"},
                {"data": "obtain_result"},
                {"data": "out_of_result"},
                {"data": "status"},
                {"data": "action",orderable: false, searchable: false}
            ];
            renderDataTable($('#education_table'),data_url,data_column);
            submitOperation(submitResponse, 'submit_basic');
            deleteOperation(deleteResponse,'delete_item',delete_url);
            editOperation(editResponse,"{{route('editEducation')}}",true);

            function submitResponse(response, this_form){
                if (response.success == true) {
                    swalSuccess(response.message);
                    $('form :input').val('');
                    clearDropify();
                    this_form.removeClass('was-validated')
                    renderDataTable($('#education_table'),data_url,data_column);
                } else {
                    swalError(response.message);
                }
            }

            function editResponse(){
                $('.dropify').dropify();
                submitOperation(submitResponse, 'submit_basic');
            }

            function deleteResponse(response){
                if(response.success == false) {
                    swalError(response.message);
                } else {
                    swalSuccess(response.message);
                    renderDataTable($('#education_table'),data_url,data_column);
                }
            }
        });
    </script>
@endsection

