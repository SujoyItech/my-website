{{--<x-app-layout>--}}
{{--    <x-slot name="header">--}}
{{--        <h2 class="font-semibold text-xl text-gray-800 leading-tight">--}}
{{--            {{ __('Profile') }}--}}
{{--        </h2>--}}
{{--    </x-slot>--}}

{{--    <div>--}}
{{--        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">--}}
{{--            @if (Laravel\Fortify\Features::canUpdateProfileInformation())--}}
{{--                @livewire('profile.update-profile-information-form')--}}

{{--                <x-jet-section-border />--}}
{{--            @endif--}}

{{--            @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))--}}
{{--                <div class="mt-10 sm:mt-0">--}}
{{--                    @livewire('profile.update-password-form')--}}
{{--                </div>--}}

{{--                <x-jet-section-border />--}}
{{--            @endif--}}

{{--            @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())--}}
{{--                <div class="mt-10 sm:mt-0">--}}
{{--                    @livewire('profile.two-factor-authentication-form')--}}
{{--                </div>--}}

{{--                <x-jet-section-border />--}}
{{--            @endif--}}

{{--            <div class="mt-10 sm:mt-0">--}}
{{--                @livewire('profile.logout-other-browser-sessions-form')--}}
{{--            </div>--}}

{{--            <x-jet-section-border />--}}

{{--            <div class="mt-10 sm:mt-0">--}}
{{--                @livewire('profile.delete-user-form')--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</x-app-layout>--}}
@extends('admin.layouts.app',['menu'=>'settings','sub_menu'=>'profileSettings','title'=>__('Profile settings')])
@section('content')

    <div class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><i class="fa fa-user-circle"></i> {{__('Profile')}}</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                    @livewire('profile.update-profile-information-form')
                    <hr>
                @endif
                @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                    <div class="mt-2">
                        @livewire('profile.update-password-form')
                    </div>
                    <hr>
                @endif
                @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                    <div class="mt-2">
                        @livewire('profile.two-factor-authentication-form')
                    </div>

                    <hr>
                @endif

                <div class="mt-2">
                    @livewire('profile.logout-other-browser-sessions-form')
                </div>

                <hr>

                <div class="mt-2">
                    @livewire('profile.delete-user-form')
                </div>
            </div>
        </div>
    </div>
@endsection
