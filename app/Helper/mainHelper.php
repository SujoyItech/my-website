<?php

use App\Models\CRM\CrmCustomer;
use App\Models\CRM\CrmQuotation;
use App\Models\Product\Product;
use App\Models\Product\ProductPricing;
use App\Models\Role\Role;
use App\Models\Role\RoleRoute;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * @param string $slug
 * @param array $data
 *
 * @return mixed
 */
function __combo($slug = '', $data = array()) {
    extract($data);
    $selected_value = isset($selected_value) && !empty($selected_value) ? $selected_value : '';
    $name = isset($name) && !empty($name) ? $name : '';
    $attributes = isset($attributes) && !empty($attributes) ? $attributes : [];
    $sql_query = isset($sql_query) && !empty($sql_query) ? $sql_query : '';
    if (!empty($slug)) {
        $combodata = DB::table('sys_dropdowns')->where('dropdown_slug', $slug)->first();
        if (!empty($combodata)) {
            $sql = $sql_query == '' ? $combodata->sqltext . ' ' . $combodata->sqlsource . ' ' . $combodata->sqlcondition : $sql_query;
            $query = DB::select($sql);
            $option_data = array();
            $attr = '';
            $multiple = isset($multiple) ? $multiple : $combodata->multiple;
            $class = $multiple == 1 ? 'form-control multi' : 'form-control';
            $attributes = empty($attributes) ? array('class' => $class, 'id' => $combodata->dropdown_name) : $attributes;
            if (!empty($attributes)) {
                foreach ($attributes as $key => $value) {
                    $attr .= $key . '="' . $value . '" ';
                }
            }

            if ($multiple == 1) {
                $attr .= 'multiple = "true"';
            } else {
                $option_data[''] = '--Select an option--';
            }
            if (empty($name)) {
                if ($multiple == 1) {
                    $name = $combodata->dropdown_name . '[]';
                } else {
                    $name = $combodata->dropdown_name;
                }
            }
            foreach ($query as $value) {
                $value_field = $combodata->value_field;
                $option_field = $combodata->option_field;
                $option_data[$value->$value_field] = $value->$option_field;
            }
            return Form::select($name, $option_data, $selected_value, (array)$attr);
        }
    }
}

/**
 * @param array|string[] $option_group
 * @param string|null $option_key
 *
 * @return mixed|object|string
 */
function __options(array $option_group = ['application_settings'], string $option_key = null) {
    $option_value = \App\Models\Setting::select('option_key', 'option_value');
    if(is_null($option_key)){
        $option_value->whereIn('option_group', $option_group);
        $options = $option_value->get();
        $result = [];
        if(!empty($options)){
            foreach ($options as $data){
                $result[$data->option_key] = $data->option_value;
            }
        }
        return (object)$result;
    }else{
        $option_value->value('option_value');
        $option_value->where('option_key', '=', $option_key);
        return !empty($option_value->first()) ? $option_value->first()->option_value : '';
    }
}

/**
 * @param string $option_group
 * @param string $option_key
 * @param string $option_value
 */
function __setOptions($option_group = '', $option_key = '', $option_value = ''){
    DB::table('sys_system_settings')
      ->updateOrInsert(
          ['option_group' => $option_group, 'option_key' => $option_key],
          ['option_value' => $option_value]
      );
}

/**
 * @param string $path
 *
 * @return string
 */
function adminAsset($path = '') {
    return asset('admin/'.$path);
}
/**
 * @param string $path
 *
 * @return string
 */
function userAsset($path = '') {
    return asset('user/'.$path);
}

function is_selected($key,$value){
    if ($key === $value) {
        return 'selected';
    }else{
        return  '';
    }
}

function buildTree($elements,$parentId = NULL,$branch=[]){
    foreach ($elements as $element){
        if ($element->parent_id == $parentId){
            $children = buildTree($elements,$element->id);
            if ($children){
                $element->children = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}

function check_permission($type){
    $module_id = \Illuminate\Support\Facades\Auth::user()->module_id;
    if ($type == SUPER_ADMIN){
        return $module_id == SUPER_ADMIN ? TRUE : FALSE;
    }elseif ($type == USER_ROLE_ADMIN){
        return $module_id == USER_ROLE_ADMIN ? TRUE : FALSE;
    }elseif ($type == PRODUCT_MANAGER_MODULE){
        return $module_id == PRODUCT_MANAGER_MODULE ? TRUE : FALSE;
    }elseif ($type == PRODUCTION_MANAGER_MODULE){
        return $module_id == PRODUCTION_MANAGER_MODULE ? TRUE : FALSE;
    }elseif ($type == CRM_MANAGER_MODULE){
        return $module_id == CRM_MANAGER_MODULE ? TRUE : FALSE;
    }else{
        return FALSE;
    }
}

function randomString($a) {
    $x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $c = strlen($x) - 1;
    $z = '';
    for ($i = 0; $i < $a; $i++) {
        $y = rand(0, $c);
        $z .= substr($x, $y, 1);
    }
    return $z;
}

function user_slugify($text) {
    $users = explode(' ', $text);
    $new_user ='';
    foreach ($users as $i => $user) {
        $new_user = $new_user.strtoupper(substr(trim($user), 0, 1));
    }
    return $new_user;
}

// get product unit price from quantity
function get_product_unit_price($product_id, $quantity)
{
    $products = ProductPricing::where('product_id' , $product_id)->get();
    $unit_price = 0;
    if (isset($products[0])) {
        foreach ($products as $price) {
            if ($price->compare == MORE) {
                if ($quantity > $price->quantity) {
                    $unit_price = $price->price;
                }
            } elseif($price->compare == EQUAL) {
                if ($quantity == $price->quantity) {
                    $unit_price = $price->price;
                }
            } elseif($price->compare == LESS) {
                if ($quantity < $price->quantity) {
                    $unit_price = $price->price;
                }
            }
        }
    }

    return $unit_price;
}

// get last id
function get_last_id($table_name)
{
    $lastId = 1;
    $item = DB::table($table_name)->orderBy('id','desc')->first();
    if(isset($item)) {
        $lastId = $item->id + $lastId;
    }

    return $lastId;
}

// check customer
function check_customer($email)
{
    $customer = CrmCustomer::where(['email' => $email])->first();
    if (isset($customer)) {
        return true;
    }
    return false;
}

// check previous assigned salesman
function checkPreviousAssignedSalesman($customer_id = null, $brand_id)
{
    if(!empty($customer_id)) {
        $quotation = CrmQuotation::where(['customer_id' => $customer_id])->orderBy('id', 'desc')->first();
        if(isset($quotation) && (!empty($quotation->assigned_salesman))) {
            return $quotation->assigned_salesman;
        }
    } else {
        $quotation = CrmQuotation::where(['brand_id' => $brand_id])->orderBy('id', 'desc')->first();
        if(isset($quotation) && (!empty($quotation->assigned_salesman))) {
            return $quotation->assigned_salesman;
        }
    }

    return false;
}

// make customer code for edit
function reference_code_edit($code)
{
    $code = explode('-',$code);
    return $code[0].'-'.$code[1];
}

function check_route_permission($route_name,$admin_permission=FALSE){
    if (\Illuminate\Support\Facades\Auth::user()->module_id == SUPER_ADMIN){
        return TRUE;
    }else{
        if ($admin_permission == TRUE){
            return TRUE;
        }else{
            $action = RoleRoute::where('name', $route_name)->orWhere('url', $route_name)->first();
            $role = Role::where('id', \Illuminate\Support\Facades\Auth::user()->role)->first();
            if (!empty($role->actions) && !empty($action)) {
                if (!empty($role->actions)) {
                    $tasks = array_filter(explode('|', $role->actions));
                }
                if (isset($tasks)) {
                    if (in_array($action->id, $tasks)) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                }
            }
        }
        return FALSE;
    }
}


if(!function_exists('create_slug')) {
    function create_slug($str = '')
    {
        $str = preg_replace('/\s\s+/', ' ', $str);
        $str = str_replace(' ', '-', $str); // Replaces all spaces with hyphens.
        $str = preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
        $str = preg_replace('/-+/', '-', $str);
        $a = substr($str, -1);
        if ($a == '-') {
            $str = rtrim($str, '-');
            //$str = clean_fields($str);
        }
        return $str;
    }
}

if(!function_exists('clean_reference')) {
    function clean_reference($str = '')
    {
        $str = str_replace(' ', '', $str); // Replaces all spaces with hyphens.
        $str = preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
        return $str;
    }
}

if(!function_exists('get_product_combination_thumbnail_and_media_url')) {
    function get_product_combination_thumbnail_and_media_url($productCombination)
    {
        if($productCombination->media_type == INTERNAL_IMAGE) {
            return ['media_url'=>Storage::url($productCombination->media_url), 'thumbnail'=>Storage::url($productCombination->media_url)];
        } else if($productCombination->media_type == EXTERNAL_IMAGE) {
            return ['media_url'=>$productCombination->media_url, 'thumbnail'=>$productCombination->media_url];
        } else if($productCombination->media_type == VIDEO_URL) {
            return ['media_url'=>$productCombination->media_url, 'thumbnail'=>adminAsset('images/play.png')];
        } else if($productCombination->media_type == _360_URL) {
            return ['media_url'=>Storage::url($productCombination->media_url), 'thumbnail'=>adminAsset('images/360.png')];
        }
        return ['media_url' => '', 'thumbnail'=>''];
    }
}

if(!function_exists('get_product_media_type')) {
    function get_product_media_type($input = null)
    {
        $output = [
            INTERNAL_IMAGE => __('Upload Image'),
            EXTERNAL_IMAGE => __('External Image Url'),
            VIDEO_URL => __('External Video Url'),
            _360_URL => __('External 360 Image Url')
        ];

        if (is_null($input)) {
            return $output;
        } else {
            return $output[$input];
        }
    }
}
