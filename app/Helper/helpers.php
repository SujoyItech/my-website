<?php


use App\Http\Boilerplate\CustomResponse;
use App\Models\AdminSetting;
use App\Models\UserSetting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

if (!function_exists('custom_number_format')) {
    /**
     * @param $value
     *
     * @return string
     */
    function custom_number_format($value) {
        if (is_integer($value)) {
            return number_format($value, 8, '.', '');
        } else if (is_string($value)) {
            $value = floatval($value);
        }
        $number = explode('.', number_format($value, 10, '.', ''));
        return $number[0] . '.' . substr($number[1], 0, 8);
    }
}

if (!function_exists('custom_number_format_with_scale')) {
    /**
     * @param $value
     * @param $scale
     *
     * @return string|null
     */
    function custom_number_format_with_scale($value, $scale) {
        return bcdiv($value, 1, $scale);
    }
}


if (!function_exists('random_string')) {
    /**
     * @param $a
     *
     * @return string
     */
    function random_string($a) {
        $x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $c = strlen($x) - 1;
        $z = '';
        for ($i = 0; $i < $a; $i++) {
            $y = rand(0, $c);
            $z .= substr($x, $y, 1);
        }
        return $z;
    }
}

if (!function_exists('random_number')) {
    /**
     * @param int $a
     *
     * @return string
     */
    function random_number($a = 10) {
        $x = '123456789';
        $c = strlen($x) - 1;
        $z = '';
        for ($i = 0; $i < $a; $i++) {
            $y = rand(0, $c);
            $z .= substr($x, $y, 1);
        }
        return $z;
    }
}

if (!function_exists('array_key_only')) {
    /**
     * @param $array
     * @param string $separator
     * @param array $exception
     *
     * @return string
     */
    function array_key_only($array, $separator = ',', $exception = []) {
        $string = '';
        $sep = '';
        foreach ($array as $key => $val) {
            if (in_array($key, $exception) == FALSE) {
                $string .= $sep . $key;
                $sep = $separator;
            }
        }
        return $string;
    }
}

if (!function_exists('storage_upload_file')) {
    /**
     * Upload file in storage according to env settings
     *
     * @param $file
     * @param $destinationPath
     * @param null $oldFile
     * @param null $width
     * @param null $height
     *
     * @return bool|string
     */
    function storage_upload_file($new_file, $path, $old_file_name = null)
    {
        if (!file_exists(Storage::url($path))) {
            mkdir(Storage::url($path), 0777, true);
        }
        if (isset($old_file_name) && $old_file_name != "" && file_exists($path . substr($old_file_name, strrpos($old_file_name, '/') + 1))) {
            unlink($path . '/' . substr($old_file_name, strrpos($old_file_name, '/') + 1));
        }

        if ($file = Storage::put($path, $new_file)) {
            return $file;
        }
        return false;
    }
}

if (!function_exists('storage_delete_file')) {
    /**
     * Delete file from storage according to env settings
     *
     * @param $destinationPath
     * @param $file
     */
    function storage_delete_file($destinationPath, $file) {
        if ($file != NULL) {
            try {
                Storage::delete($destinationPath . $file);
            } catch (Exception $e) {

            }
        }
    }
}

if (!function_exists('upload_file')) {
    /**
     * Upload file in public Dir
     *
     * @param $file
     * @param $destinationPath
     * @param null $oldFile
     * @param null $width
     * @param null $height
     *
     * @return bool|string
     */
    function upload_file($new_file, $path, $old_file_name = null, $width = null, $height = null)
    {
        if (!file_exists(public_path($path))) {
            mkdir(public_path($path), 0777, true);
        }
        if (isset($old_file_name) && $old_file_name != "" && file_exists($path . substr($old_file_name, strrpos($old_file_name, '/') + 1))) {

            unlink($path . '/' . substr($old_file_name, strrpos($old_file_name, '/') + 1));
        }

        $input['image_name'] = uniqid() . time() . '.' . $new_file->getClientOriginalExtension();
        $imgPath = public_path($path . $input['image_name']);

        $makeImg = Image::make($new_file);
        if ($width != null && $height != null && is_int($width) && is_int($height)) {
            $makeImg->resize($width, $height);
            $makeImg->fit($width, $height);
        }

        if ($makeImg->save($imgPath)) {
            return $input['image_name'];
        }
        return false;

    }
}

if (!function_exists('delete_file')) {
    /**
     * Remove file from public Dir
     * @param $destinationPath
     * @param $file
     */
    function delete_file($destinationPath, $file) {
        if ($file != NULL) {
            try {
                Storage::disk('public')->delete($destinationPath . $file);
            } catch (Exception $e) {

            }
        }
    }
}

if (!function_exists('remove_image')) {
    /**
     * @param $path
     * @param $file_name
     */
    function remove_image($path, $file_name) {
        if (isset($file_name) && $file_name != "" && file_exists($path . $file_name)) {
            unlink($path . $file_name);
        }
    }
}

if (!function_exists('convert_currency')) {
    /**
     * @param $amount
     * @param string $to
     * @param string $from
     *
     * @return float|int
     */
    function convert_currency($amount, $to = 'USD', $from = 'USD') {
        try {
            $url = "https://min-api.cryptocompare.com/data/price?fsym=$from&tsyms=$to";
            $json = file_get_contents($url); //,FALSE,$ctx);
            $jsonData = json_decode($json, TRUE);
            return $amount * $jsonData[$to];
        } catch (Exception $e) {
            return 0;
        }
    }
}

if (!function_exists('get_clientIp')) {
    /**
     * @return mixed|string
     */
    function get_clientIp() {
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
    }
}

if (!function_exists('language')) {
    /**
     * @return array|bool
     */
    function language() {
        $lang = [];
        $path = base_path('resources/lang');
        foreach (glob($path . '/*.json') as $file) {
            $langName = basename($file, '.json');
            $lang[$langName] = $langName;
        }
        return empty($lang) ? FALSE : $lang;
    }
}

if (!function_exists('lang_name')) {
    /**
     * @param null $input
     *
     * @return array|mixed
     */
    function lang_name($input = NULL) {
        $output = [
            'en' => __('English'),
            'pt' => __('Portuguese'),
            'es' => __('Spanish'),
            'fr' => __('French'),
            'ru' => __('Russian'),
        ];
        try {
            if (is_null($input)) {
                return $output;
            } else {
                return $output[$input];
            }
        } catch (Exception $e) {
            return $output['en'];
        }

    }
}

if (!function_exists('translate')) {
    /**
     * @param $str
     *
     * @return mixed|string
     */
    function translate($str) {
        $str = trim($str);
        $lan = get_user_lan();
        $path = 'resources/lang/' . $lan . '.json';
        $jsonString = file_get_contents(base_path($path));
        $data = json_decode($jsonString, TRUE);
        return isset($data[$str]) ? $data[$str] : $str;
    }
}


if (!function_exists('set_lang')) {
    /**
     * @param $lang
     */
    function set_lang($lang) {
        $default = settings('lang');
        $lang = strtolower($lang);
        $languages = language();
        if (in_array($lang, $languages)) {
            app()->setLocale($lang);
        } else {
            if (isset($default)) {
                $lang = $default;
                app()->setLocale($lang);
            }
        }
    }
}

if (!function_exists('odd_even')) {
    /**
     * @param $number
     *
     * @return string
     */
    function odd_even($number) {
        return $number % 2 == 0 ? 'even' : 'odd';
    }
}

if (!function_exists('convert_currency')) {
    /**
     * @param $amount
     * @param string $to
     * @param string $from
     *
     * @return string
     */
    function convert_currency($amount, $to = 'USD', $from = 'USD') {
        $url = "https://min-api.cryptocompare.com/data/price?fsym=$from&tsyms=$to";
        $json = file_get_contents($url); //,FALSE,$ctx);
        $jsonData = json_decode($json, TRUE);
        return bcmul($amount, $jsonData[$to]);
    }
}

if (!function_exists('get_user_lan')) {
    /**
     * @return string
     */
    function get_user_lan() {
        try {
            $obj = UserSetting::where(['user_id' => Auth::id()])->first();
            return $obj->language;
        } catch (Exception $e) {
            return 'en';
        }
    }
}

if (!function_exists('get_total_with_percentage')) {
    /**
     * @param $amount
     * @param $percentage
     *
     * @return float
     */
    function get_total_with_percentage($amount, $percentage) {
        $amount += get_percentage($amount, $percentage);
        return (float)$amount;
    }
}

if (!function_exists('get_percentage')) {
    /**
     * @param $amount
     * @param $percentage
     *
     * @return float
     */
    function get_percentage($amount, $percentage) {
        return (float)($amount * ($percentage / 100));
    }
}

if (!function_exists('this_day')) {
    /**
     * @return string
     */
    function this_day() {
        return Carbon::now()->startOfDay()->format(MYSQL_DATE_WITHOUT_TIME);
    }
}

if (!function_exists('last_day')) {
    /**
     * @return string
     */
    function last_day() {
        return Carbon::now()->subDay()->startOfDay()->format(MYSQL_DATE_WITHOUT_TIME);
    }
}

if (!function_exists('next_day')) {
    /**
     * @return string
     */
    function next_day() {
        return Carbon::now()->addDay()->startOfDay()->format(MYSQL_DATE_WITHOUT_TIME);
    }
}

if (!function_exists('this_month')) {
    /**
     * @return string
     */
    function this_month() {
        return Carbon::now()->startOfMonth()->format(MYSQL_DATE_WITHOUT_TIME);
    }
}


if (!function_exists('next_month')) {
    /**
     * @return string
     */
    function next_month() {
        return Carbon::now()->addMonth()->startOfMonth()->format(MYSQL_DATE_WITHOUT_TIME);
    }
}

if (!function_exists('this_year')) {
    /**
     * @return string
     */
    function this_year() {
        return Carbon::now()->startOfYear()->format(MYSQL_DATE_WITHOUT_TIME);
    }
}

if (!function_exists('next_year')) {
    /**
     * @return string
     */
    function next_year() {
        return Carbon::now()->addYear()->startOfYear()->format(MYSQL_DATE_WITHOUT_TIME);
    }
}

if (!function_exists('custom_encrypt')) {
    /**
     * @param $string
     *
     * @return string
     */
    function custom_encrypt($string) {
        $key = env('APP_KEY');
        $result = '';
        for ($i = 0, $k = strlen($string); $i < $k; $i++) {
            $char = substr($string, $i, 1);
            $keyChar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) + ord($keyChar));
            $result .= $char;
        }
        return base64_encode($result);
    }
}

if (!function_exists('custom_decrypt')) {
    /**
     * @param $string
     *
     * @return string
     */
    function custom_decrypt($string) {
        $key = env('APP_KEY');
        $result = '';
        $string = base64_decode($string);
        for ($i = 0, $k = strlen($string); $i < $k; $i++) {
            $char = substr($string, $i, 1);
            $keyChar = substr($key, ($i % strlen($key)) - 1, 1);
            $char = chr(ord($char) - ord($keyChar));
            $result .= $char;
        }
        return $result;
    }
}

if (!function_exists('encrypt_single_column_value')) {
    /**
     * @param $arr
     * @param $columnForEncryption
     * @param $customEncryption
     *
     * @return array
     */
    function encrypt_single_column_value($arr, $columnForEncryption, $customEncryption) {
        $data = [];
        foreach ($arr as $key => $object) {
            $temp = [];
            foreach ($object->getOriginal() as $column => $value) {
                //            if($column == $columnForEncryption) {
                if (in_array($column, $columnForEncryption)) {
                    $index = array_search($column, $columnForEncryption);
                    if ($customEncryption[$index]) {
                        $temp[$column] = custom_encrypt($value);
                    } else {
                        $temp[$column] = encrypt($value);
                    }
                } else {
                    $temp[$column] = $value;
                }
                $data[$key] = (object)$temp;
            }
        }
        return $data;
    }
}

if (!function_exists('upload_base64_image')) {
    /**
     * @param $base64String
     * @param $imagePath
     *
     * @return string
     */
    function upload_base64_image($base64String, $imagePath) {
        $image = $base64String;
        $image = str_replace(' ', '+', $image);
        $imageName = uniqid() . '.jpeg';
        $path = 'uploads/' . $imagePath;
        if (!file_exists($path)) {
            mkdir($path, '755');
        }
        Image::make(file_get_contents($image))->save($path . $imageName);

        return $imageName;
    }
}

if (!function_exists('get_percentile')) {
    /**
     * @param $part
     * @param $total
     *
     * @return float
     */
    function get_percentile($part, $total) {
        return (float)(($part / $total) * 100);
    }
}
if (!function_exists('logo_image_path')) {
    /**
     * @return string
     */
    // image path
    function logo_image_path()
    {
        return 'uploads/logo/';
    }
}
if (!function_exists('admin_image_path')) {
    /**
     * @return string
     */
    // image path
    function admin_image_path()
    {
        return 'uploads/admin/';
    }
}

if (!function_exists('coin_icon_path')) {
    /**
     * @return string
     */
    // image path
    function coin_icon_path()
    {
        return 'uploads/coin/';
    }
}

if (!function_exists('get_image_path')){
    function get_image_path($type=''){
        if ($type == 'user'){
            return 'admin/images/users/';
        }if ($type == 'skill'){
            return 'admin/images/personal/skill/';
        }elseif ($type == 'brand'){
            return 'admin/images/products/brand/';
        }elseif ($type == 'category'){
            return 'admin/images/products/category/';
        }elseif ($type == 'settings'){
            return  'admin/images/application/settings/';
        }
    }
}

if (! function_exists('jsonResponse')) {
    /**
     * Return a new response from the application.
     *
     * @param bool $type
     *
     * @return string
     */
    function jsonResponse($type=true)
    {
        return new CustomResponse($type);
    }
}

if (! function_exists('getResponseMessage')) {
    /**
     * Return a new response from the application.
     *
     * @param bool $type
     *
     * @param string $message
     *
     * @return array
     */
    function getResponseMessage(bool $type=false, string $message='Something went wrong')
    {
        return [
            'success' => $type,
            'message' => $message,
            'data' => []
         ];
    }
}

if (! function_exists('sendResponse')) {

    function sendResponse($data = '', $message = '')
    {
        if (!empty($data)) {
            $response = [
                'success' => true,
                'message' => $message,
                'data' => $data,
            ];
        } else {
            $response = [
                'success' => true,
                'message' => $message,
            ];
        }

        return response()->json($response, 200);
    }
}

if (! function_exists('sendResponseError')) {

    /**
     * @param $errorCode
     * @param $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    function sendResponseError($errorCode = '', $error = '', $errorMessages = [], $code = 200)
    {
        if (!$error) {
            $error = __('Something went wrong.');
        }
        $response = [
            'success' => false,
            'code' => intval($errorCode),
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}

if (! function_exists('sendError')) {

    /**
     * @param $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}

if (!function_exists('storage_logs')) {
    /**
     * @return string
     */
    // image path
    function storage_logs()
    {
        return storage_path().'/logs/';
    }
}

if (!function_exists('get_coin_amount')) {

    function get_coin_amount($value, $type)
    {
        if ($type === MULTIPLY) {
            return $value * COIN_CONSTANT_AMOUNT;
        } else if ($type === DIVISION) {
            return number_format($value / COIN_CONSTANT_AMOUNT, 8, '.', '');
        } else {
            return 0;
        }
    }
}

if (!function_exists('app_languages')) {

    function app_languages()
    {
        return app(JoeDixon\Translation\Drivers\Translation::class)->allLanguages();
    }
}

if (!function_exists('easy_date_format')) {

    function easy_date_format($dateTime)
    {
        return date('d M Y | g:i a', strtotime($dateTime ?? ''));
    }
}

if (!function_exists('calculateFees')) {
    function calculateFees($amount, $feeMethod, $feePercentage, $feeFixed)
    {
        try {
            if ($feeMethod == FEES_METHOD_FIXED) {
                return customNumberFormat($feeFixed);
            } elseif ($feeMethod == FEES_METHOD_PERCENTAGE) {
                return customNumberFormat(bcdiv(bcmul($feePercentage, $amount, COIN_DECIMAL_SCALE), 100, COIN_DECIMAL_SCALE));
            } elseif ($feeMethod == FEES_METHOD_BOTH) {
                return customNumberFormat(bcadd($feeFixed, bcdiv(bcmul($feePercentage, $amount, COIN_DECIMAL_SCALE), 100, COIN_DECIMAL_SCALE), COIN_DECIMAL_SCALE));
            } else {
                return 0;
            }
        } catch (\Exception $e) {
            return 0;
        }
    }
}

/**
 * Description: Customize number format
 * @param $value
 * @return string
 */
if (!function_exists('customNumberFormat')) {
    function customNumberFormat($value)
    {
        $number = explode('.', $value);
        if (!isset($number[1])) {
            return number_format($value, 8, '.', '');
        } else {
            $result = substr($number[1], 0, 8);
            if (strlen($result) < 8) {
                $result = number_format($value, 8, '.', '');
            } else {
                $result = $number[0] . "." . $result;
            }

            return $result;
        }
    }

    if(!function_exists('wallet_record_types')) {
        function wallet_record_types($input = null)
        {
            $output = [
                CREDIT => __('Credit'),
                DEBIT => __('Debit'),
            ];

            if (is_null($input)) {
                return $output;
            } else {
                return $output[$input];
            }
        }
    }

    if(!function_exists('transaction_status')) {
        function transaction_status($input = null)
        {
            $output = [
                PROCESSING => __('Processing'),
                CONFIRMED => __('Confirmed'),
                NOTIFIED => __('Notified'),
                COMPLETED => __('Completed'),
                FAILED => __('Failed'),
                REJECTED => __('Rejected'),
                EXPIRED => __('Expired'),
                BLOCKED => __('Blocked'),
                REFUNDED => __('Refunded'),
                VOIDED => __('Voided')
            ];

            if (is_null($input)) {
                return $output;
            } else {
                return $output[$input];
            }
        }
    }

    if(!function_exists('withdraw_category')) {
        function withdraw_category($input = null)
        {
            $output = [
                INTERNAL_ADDRESS => __('Internal Transfer'),
                EXTERNAL_ADDRESS => __('External Transfer')
            ];

            if (is_null($input)) {
                return $output;
            } else {
                return $output[$input];
            }
        }
    }

    if(!function_exists('internal_txid')) {
        function internal_txid()
        {
            $txid = random_string(64);
            while(true) {
                $txExists = \App\Models\Withdrawal::where(['transaction_hash' => $txid])->select('id')->first();
                if (!$txExists) {
                    $txExists = \App\Models\Deposit::where(['transaction_hash' => $txid])->select('id')->first();
                    if(!$txExists) {
                        break;
                    }
                }
                $txid = random_string(64);
            }
            return $txid;
        }
    }

    if(!function_exists('get_decimal_point_count')) {
        function get_decimal_point_count($float)
        {
            //$working = number_format($float,100);
            $working = rtrim($float,"0");
            $working = explode(".",$working);
            $working = $working[1];
            return strlen($working);
        }
    }

    if(!function_exists('withdraw_status')) {
        function withdraw_deposit_status($input = null)
        {
            $output = [
                STATUS_PROCESSING => __('Processing'),
                STATUS_SUCCESS => __('Done'),
                STATUS_ADMIN_APPROVAL => __('Pending for Admin Approval'),
                STATUS_CANCELLED => __('Cancelled'),
                STATUS_FAILED => __('Failed')
            ];

            if (is_null($input)) {
                return $output;
            } else {
                return $output[$input];
            }
        }
    }
}
