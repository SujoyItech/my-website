<?php

const SUPER_ADMIN=0;
const USER_ROLE_ADMIN=1;
const PRODUCT_MANAGER_MODULE = 2;
const PRODUCTION_MANAGER_MODULE = 3;
const CRM_MANAGER_MODULE = 4;
const WEBSITE_MANAGER_MODULE = 5;

const FRONT_END = 0;
const BACK_END = 1;
const DATABASE = 2;
const DEVOPS = 3;
const OTHERS = 10;

const BEGINNERS =0;
const AVERAGE =1;
const SKILLED =2;
const SPECIALIST =3;
const EXPERT =4;

const PERSONAL_EXPERIENCE = 0;
const COMPANY_EXPERIENCE = 1;
const ONLINE_COURSE = 2;

const GLOBAL_PAGINATION = 20;
const MYSQL_DATE_FORMAT = 'Y-m-d h:i:s';
const MYSQL_DATE_WITHOUT_TIME = 'Y-m-d';

//users table created_by column value definition
const SEEDUSER = 0;

//status
const INACTIVE = 0;
const ACTIVE = 1;
const USER_SUSPENDED = 2;
const USER_BLOCKED = 3;

const BLOCK = 0;
const UNBLOCK = 1;



const STATUS_PROCESSING = 0;
const STATUS_ACTIVE = 1;
const STATUS_SUCCESS = 1;
const STATUS_ADMIN_APPROVAL = 2;
const STATUS_CANCELLED = 3;
const STATUS_FAILED = 4;


//transaction status
const PROCESSING = 0;
const CONFIRMED = 1;
const NOTIFIED = 2;
const COMPLETED = 3;
const FAILED = 4;
const REJECTED = 5;
const EXPIRED = 6;
const BLOCKED = 7;
const REFUNDED = 8;
const VOIDED = 9;

