<?php

namespace App\Http\Controllers\Web\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\User\UserRequest;
use App\Http\Services\User\UserService;
use App\Models\Role\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /*
  * @var SupplierService
  */
    private $userService;

    /**
     * SupplierController constructor.
     *
     * @param UserService $service
     */
    public function __construct(UserService $service){
        $this->userService = $service;
    }

    public function index(Request $request){
        $user_lists = $this->userService->getUserList();
        $data['roles'] = Role::all();
        if ($request->ajax()){
            return datatables($user_lists)
                ->editColumn('module_id',function ($item){
                    if ($item->module_id == USER_ROLE_ADMIN) return 'Admin';
                    elseif ($item->module_id == PRODUCT_MANAGER_MODULE) return 'Product Manager';
                    elseif ($item->module_id == PRODUCTION_MANAGER_MODULE) return 'Production Manager';
                    elseif ($item->module_id == CRM_MANAGER_MODULE) return 'CRM Manager';
                    elseif ($item->module_id == WEBSITE_MANAGER_MODULE) return 'Website Manager';
                })->editColumn('status',function ($item){
                    if($item->status == ACTIVE ) return '<span class="badge badge-success">'.__('Active').'</span>';
                    elseif($item->status == INACTIVE ) return '<span class="badge badge-primary">'.__('Inactive').'</span>';
                    elseif($item->status == USER_SUSPENDED ) return '<span class="badge badge-warning">'.__('Suspended').'</span>';
                    elseif($item->status == USER_BLOCKED ) return '<span class="badge badge-danger">'.__('Blocked').'</span>';
                })->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['status','action'])
                ->make(TRUE);
        }
        return view('admin.users.users',$data);
    }

    public function userSlugCheck(Request $request){
        return $this->userService->checkSlug($request->slug,$request->id);
    }

    public function store(UserRequest $request){
        if(!empty($request->id)){
            return $this->userService->update($request->id,$request->except('id'));
        }else{
            return $this->userService->create($request->except('id'));
        }
    }

    public function edit(Request $request){
        return view('admin.users.user_add',$this->userService->getUserData($request->id));
    }

    public function delete(Request $request){
        return $this->userService->delete($request->id);
    }
}
