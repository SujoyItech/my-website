<?php

namespace App\Http\Controllers\Web\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Services\SettingService;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    private $settingService;

    public function __construct(SettingService $service){
        $this->settingService = $service;
    }
    public function siteSetting(){
        $data['settings'] = __options(['site_settings']);
        return view('admin.settings.site_setting',$data);
    }

    public function logoSetting(){
        $data['settings'] = __options(['logo_settings']);
        return view('admin.settings.logo_setting',$data);
    }

    public function socialSetting(){
        $data['settings'] = __options(['social_settings']);
        return view('admin.settings.social_setting',$data);
    }

    public function adminSettingsSave(Request $request){
        if ($request->option_type == 'text'){
            $this->insert_or_update($request->option_group,$request->option_key,$request->option_value);
        }elseif($request->option_type == 'file'){
            $check_logo = Setting::where(['option_key'=>$request->option_key,'option_group'=>$request->option_group])->first();
            $option_value = upload_file($request->option_value,'admin/images/application/settings/',$check_logo->option_value ?? '');
            $this->insert_or_update($request->option_group,$request->option_key,$option_value);
        }
    }

    private function insert_or_update($option_group,$option_key,$option_value){
        Setting::updateOrCreate(['option_group'=>$option_group,'option_key'=>$option_key],
            ['option_group'=>$option_group,'option_key'=>$option_key,'option_value'=>$option_value]);
    }
}
