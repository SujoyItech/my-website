<?php

namespace App\Http\Controllers\Web\Admin\Personal;

use App\Http\Controllers\Controller;
use App\Http\Services\Personal\SkillService;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    private $skillService;

    public function __construct(SkillService $service){
        $this->skillService = $service;
    }

    public function index(Request $request){
        $skill_lists = $this->skillService->getData([],[],['created_at'=>'desc']);
        if ($request->ajax()){
            return datatables($skill_lists)
                ->editColumn('icon',function ($item){
                    return '<img src="'.asset(get_image_path('skill').'/'.$item->icon).'" class="img-circle" width="30">';
                })->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '';
                    $html .= '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['icon','status','action'])
                ->make(TRUE);
        }

        return view('admin.personal.skills.skills');
    }
    public function edit(Request $request){
        return view('admin.personal.skills.skill_add',$this->skillService->getSkillData($request->id));
    }

    public function store(Request $request){
        if(!empty($request->id)){
            return $this->skillService->update($request->id,$request->except('id'));
        }else{
            return $this->skillService->create($request->except('id'));
        }

    }

    public function delete(Request $request){
        return $this->skillService->delete($request->id);
    }
}
