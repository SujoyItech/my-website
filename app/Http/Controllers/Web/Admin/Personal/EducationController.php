<?php

namespace App\Http\Controllers\Web\Admin\Personal;

use App\Http\Controllers\Controller;
use App\Http\Requests\Personal\EducationRequest;
use App\Http\Services\Personal\EducationService;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    private $educationService;

    public function __construct(EducationService $service){
        $this->educationService = $service;
    }

    public function index(Request $request){
        $education_lists = $this->educationService->getData([],[],['created_at'=>'desc']);
        if ($request->ajax()){
            return datatables($education_lists)
                ->editColumn('icon',function ($item){
                    return '<img src="'.asset(get_image_path('education').'/'.$item->icon).'" class="img-circle" width="30">';
                })->editColumn('status',function ($item){
                    return $item->status == STATUS_ACTIVE ? '<span class="badge badge-success">'.__('Active').'</span>':'<span class="badge badge-warning">'.__('Inactive').'</span>';
                }) ->editColumn('action',function ($item){
                    $html = '<a href="javascript:void(0)" class="text-success p-1 download_item" data-id="'.$item->id.'"><i class="fa fa-download"></i></a>';
                    $html .= '<a href="javascript:void(0)" class="text-info p-1 edit_item" data-id="'.$item->id.'"><i class="fa fa-edit"></i></a>';
                    $html .='<a href="javascript:void(0)" class="text-danger p-1 delete_item" data-id="'.$item->id.'"><i class="fa fa-trash"></i></a>';
                    return $html;
                })->rawColumns(['icon','status','action'])
                ->make(TRUE);
        }

        return view('admin.personal.educations.educations');
    }
    public function edit(Request $request){
        return view('admin.personal.educations.education_add',$this->educationService->getEducationData($request->id));
    }

    public function store(Request $request){
        if(!empty($request->id)){
            return $this->educationService->update($request->id,$request->except('id'));
        }else{
            return $this->educationService->create($request->except('id'));
        }

    }

    public function delete(Request $request){
        return $this->educationService->delete($request->id);
    }
}
