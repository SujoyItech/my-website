<?php

namespace App\Http\Controllers\Web\Admin\Personal;


use App\Http\Controllers\Controller;
use App\Http\Services\DashboardService;
use Illuminate\Http\Request;


class DashboardController extends Controller {

    protected $dashboardService;

    public function __construct(DashboardService $dashboardService) {
        $this->dashboardService = $dashboardService;
    }

    public function index(Request $request){
        return view('admin.products.dashboard.home');
    }

}
