<?php

namespace App\Http\Controllers\Web\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Auth\ForgetPasswordRequest;
use App\Http\Requests\Web\Auth\ForgotPasswordResetRequest;
use App\Http\Services\Auth\AuthService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private $authService;

    /**
     * SupplierController constructor.
     *
     * @param AuthService $service
     */
    public function __construct(AuthService $service){
        $this->authService = $service;
    }
    public function login() {
        if (Auth::user()) {
            return redirect()->route('adminHome');
        } else {
            return redirect()->route('login');
        }
    }

    public function forgetPassword() {
        return view('auth.forgot-password_1');
    }

    public function sendForgetPasswordMail(ForgetPasswordRequest $request) {
        $response = $this->authService->sendForgotPasswordMail($request);
        if ($response->getStatus() == TRUE) {
            return redirect()->route('login')->with('success', $response->getMessage());
        } else {
            return redirect()->back()->with('dismiss', $response->getMessage());
        }
    }

    public function resetPassword($reset_code){
        $remember_token = decrypt($reset_code);
        $response = $this->authService->resetPassword($remember_token);
        if ($response->getStatus() == TRUE){
            $data['remember_token'] = $remember_token;
            return view('auth.reset-password_1', $data);
        }else{
            return redirect()->route('login')->with(['dismiss'=>$response->getMessage()]);
        }
    }

    public function changePassword(ForgotPasswordResetRequest $request){
        $response = $this->authService->changePassword($request);
        if ($response->getStatus() == TRUE) {
            return redirect()->route('login')->with('success', $response->getMessage());
        } else {
            return redirect()->route('login')->with('dismiss', $response->getMessage());
        }
    }
}
