<?php

namespace App\Http\Controllers\Api\CRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CRM\QuotationRequest;
use App\Http\Services\CRM\QuotationService;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    private $quotationService;
    public function __construct(QuotationService $service)
    {
        $this->quotationService = $service;
    }

    //quotation request
    public function quotationRequest(QuotationRequest $request)
    {
        return $this->quotationService->create($request);
    }
}
