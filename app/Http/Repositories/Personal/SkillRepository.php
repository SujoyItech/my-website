<?php

namespace App\Http\Repositories\Personal;

use App\Http\Repositories\BaseRepository;
use App\Models\Personal\Skill;

class SkillRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Personal/Skill $model
       */
    public function __construct(Skill $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
