<?php

namespace App\Http\Repositories\Personal;

use App\Http\Repositories\BaseRepository;
use App\Models\Personal\Education;

class EducationRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Personal/Education $model
       */
    public function __construct(Education $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository
}
