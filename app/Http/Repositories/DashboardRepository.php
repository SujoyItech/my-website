<?php

namespace App\Http\Repositories;

use App\Models\Coin;
use App\Models\Deposit;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Withdrawal;

class DashboardRepository
{
    public function totalMerchants() {
        return User::where('role_id', MERCHANT)->count();
    }

    public function totalCoins() {
        return Coin::count();
    }

    public function totalTransactions() {
        return Transaction::where('status', CONFIRMED)->count();
    }

    public function totalWithdrawals() {
        return Withdrawal::where('status', STATUS_SUCCESS)->count();
    }

    public function totalDeposits() {
        return Deposit::where('status', STATUS_SUCCESS)->count();
    }

    public function totalBTCRevenue() {
        $revenue = Transaction::where('status', CONFIRMED)->sum('fees');
        $revenue += Withdrawal::where('status', STATUS_SUCCESS)->sum('fees');

        return get_coin_amount($revenue, DIVISION);
    }
}
