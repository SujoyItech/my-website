<?php

namespace App\Http\Repositories\Product;

use App\Http\Repositories\BaseRepository;
use App\Models\Product\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param Product/Category $model
       */
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getCategoryDetails($id){
        return $this->model::where(['id'=>$id])->first();
    }

    public function categoryOrderUpdate(Request $request){
        $parent_id = (int)$request->parent_id;
        $position = (int)$request->position;
        $category_id = (int)$request->id;
        $categories = Category::where('parent_id', $parent_id)
                              ->orderBy('sort_number', 'asc')
                              ->pluck('id')->toArray();
        $dif_category = array_diff($categories, [$category_id]);
        $dif_sort_category = array_values($dif_category);
        $new_category_arr = [];
        $new_key = 0;
        $i = 0;
        if (!empty($dif_sort_category)) {
            foreach ($dif_sort_category as $i => $dif_sort_menu) {
                if ($i == $position) {
                    $new_category_arr[$position] = $category_id;
                    $new_key++;
                }
                $new_category_arr[$new_key] = $dif_sort_category[$i];
                $new_key++;
            }
            if ($position > $i) {
                $new_category_arr[$position] = $category_id;
            }
        }

        $sqltext = 'UPDATE categories SET sort_number = CASE';
        if (!empty($dif_sort_category)) {
            foreach ($new_category_arr as $key => $new_category_id) {
                $sqltext .= ' WHEN id = ' . $new_category_id . ' THEN ' . $key;
            }
        } else {
            $sqltext .= ' WHEN id = ' . $category_id . ' THEN ' . 1;
        }

        $sqltext .= ' ELSE sort_number END ,';
        $sqltext .= ' parent_id = CASE WHEN id = ' . $category_id . ' THEN ' . $parent_id . ' ELSE parent_id END';
        return DB::statement($sqltext);
    }

}
