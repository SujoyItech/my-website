<?php

namespace App\Http\Repositories;

use Spatie\Activitylog\Models\Activity;

class ActivityLogRepository extends BaseRepository
{

    /**
     * ActivityLogRepository constructor.
     *
     * @param Activity $model
     */
    public function __construct(Activity $model)
    {
        parent::__construct($model);
    }

    /**
     * @param mixed $columnToSort
     * @param mixed $orderBy
     *
     * @return [type]
     */
    public function getActivityLogs($columnToSort, $orderBy) {
        return $this->model->with('causer')
            ->orderBy($columnToSort, $orderBy)
            ->paginate(config('paginate'));
    }

    /**
     * @param mixed $columnToSort
     * @param mixed $orderBy
     * @param mixed $query
     *
     * @return [type]
     */
    public function searchActivityLogs($columnToSort, $orderBy, $query) {
        return $this->model->join('users', 'users.id', '=', 'activity_log.causer_id')
            ->select(
                'activity_log.id',
                'activity_log.log_name',
                'activity_log.description',
                'activity_log.subject_type',
                'activity_log.created_at',
                'users.email'
            )
            ->where('activity_log.log_name', 'LIKE', '%' . $query . '%')
            ->orWhere('activity_log.description', 'LIKE', '%' . $query . '%')
            ->orWhere('activity_log.subject_type', 'LIKE', '%' . $query . '%')
            ->where('users.email', 'LIKE', '%' . $query . '%')
            ->orderBy('activity_log.'.$columnToSort.'', $orderBy)
            ->paginate(config('paginate'));
    }
}
