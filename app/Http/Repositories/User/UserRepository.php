<?php

namespace App\Http\Repositories\User;

use App\Http\Repositories\BaseRepository;
use App\Http\Services\MailService;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository extends BaseRepository
{
    /**
       * Instantiate repository
       *
       * @param User/User $model
       */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    // Your methods for repository

    public function getUserList(){
       return $this->model::select('users.*','roles.title as role_name')
                          ->leftJoin('roles','users.role','roles.id')
                          ->where('users.module_id','<>',SUPER_ADMIN)
                          ->where('users.id','<>',Auth::user()->id)
                          ->orderBy('users.created_at','desc');
    }
    public function getSalesmanList(){
       return $this->model::where('status', STATUS_ACTIVE)
                            ->where(function ($query) {
                              $query->where('role', '=', ROLE_SALESMANAGER)
                                  ->orWhere('role', '=', ROLE_SALESMAN);
                          })->select('users.*')
                          ->orderBy('users.created_at','desc');
    }

    public function getUserDetails($id){
        return $this->model::where('id',$id)->first();
    }

    public function userPasswordChangeMail($user) {
        if ($user) {
            $userName = $user->name;
            $userEmail = $user->email;
            $subject = __('Reset Password');
            $data['name'] = $userName;
            $data['remember_token'] = $user->remember_token;
            MailService::sendResetPasswordMailProcess($userEmail, $data, $subject);
        }
    }
}
