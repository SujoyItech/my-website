<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->module_id == SUPER_ADMIN || Auth::user()->module_id == USER_ROLE_ADMIN){
            return $next($request);
        }
        return redirect()->route('permissionDenied');
    }
}
