<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class WebsiteManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (check_permission(SUPER_ADMIN) ||check_permission(USER_ROLE_ADMIN) || check_permission(WEBSITE_MANAGER_MODULE)){
            return $next($request);
        }
        return redirect()->route('permissionDenied');
    }
}
