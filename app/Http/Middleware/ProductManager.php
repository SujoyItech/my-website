<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (check_permission(SUPER_ADMIN) ||check_permission(USER_ROLE_ADMIN) || check_permission(PRODUCT_MANAGER_MODULE)){
            return $next($request);
        }
        return redirect()->route('permissionDenied');
    }

}
