<?php

namespace App\Http\Middleware;

use App\Models\Role\Role;
use App\Models\Role\RoleRoute;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RolePermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next) {
        $user = Auth::user();
        if (empty(request()->route()->getName())) {
            $url = strtok($request->getRequestUri(), '?');
        } else {
            $url = request()->route()->getName();
        }

        if ($user->module_id == SUPER_ADMIN){
            return $next($request);
        }else if ($this->checkRolePermission($url, $user->role)) {
            return $next($request);
        }else{
            return redirect()->route('permissionDenied')->with('dismiss', 'Permission denied');
        }
    }

    public function checkRolePermission($userAction, $userRole) {

        $action = RoleRoute::where('name', $userAction)->orWhere('url', $userAction)->first();
        $role = Role::where('id', $userRole)->first();
        if (!empty($role->actions) && !empty($action)) {
            if (!empty($role->actions)) {
                $tasks = array_filter(explode('|', $role->actions));
            }
            if (isset($tasks)) {
                if (in_array($action->id, $tasks)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
        return FALSE;
    }
}
