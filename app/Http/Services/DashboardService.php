<?php

namespace App\Http\Services;

use App\Http\Repositories\DashboardRepository;

class DashboardService extends BaseService
{
    protected $dashRepo;
    /**
     * Instantiate repository
     *
     * @param DashboardRepository $repository
     */
    public function __construct(DashboardRepository $repository)
    {
        $this->repo = $repository;
    }

    public function dashData() {
        $data['merchants'] = $this->repo->totalMerchants();
        $data['transactions'] = $this->repo->totalTransactions();
        $data['coins'] = $this->repo->totalCoins();
        $data['withdrawals'] = $this->repo->totalWithdrawals();
        $data['deposits'] = $this->repo->totalDeposits();
        return $data;
    }
}
