<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Mail;

class MailService extends BaseService
{
    public static function sendResetPasswordMailProcess($email, $body, $subject){
        Mail::send('admin.mail.email.reset_password_mail', ['body' => $body], function ($messages) use ($email, $subject) {
            $messages->to($email)->subject($subject);
        });
    }
}
