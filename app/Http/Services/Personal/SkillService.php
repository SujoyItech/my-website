<?php

namespace App\Http\Services\Personal;

use App\Http\Repositories\Personal\SkillRepository;
use App\Http\Services\BaseService;

class SkillService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param Personal/SkillRepository $repository
     */
    public function __construct(SkillRepository $repository)
    {
        $this->repo = $repository;
    }

    public function getSkillData($id){

        if ($id !== NULL){
            $data['skill'] = $this->repo->firstWhere(['id'=>$id]);
        }else{
            $data['skill'] = [];
        }
        return $data;
    }

    public function create(array $requestArray) {
        try {
            if (isset($requestArray['icon']) && !empty($requestArray['icon'])){
                $requestArray['icon'] = $this->imageData($requestArray['icon']);
            }
            $delivery = $this->repo->create($requestArray);
            if ( $delivery) {
                return jsonResponse(true)->message(__("Skill has been created successfully."));
            }
            return jsonResponse(false)->message(__("Skill create failed."));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            if (isset($requestArray['icon']) && !empty($requestArray['icon'])){
                $requestArray['icon'] = $this->imageData($requestArray['icon'],$id);
            }
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("Skill has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    private function imageData($image,$id=NULL){
        if ($id !== NULL){
            $details =  $this->repo->firstWhere(['id'=>$id]);
            return upload_file($image,get_image_path('Skill'),$details->icon ?? '');
        }else{
            return upload_file($image,get_image_path('Skill'));
        }
    }

    public function delete($id){
        try {
            $supplier = $this->repo->destroy($id);
            if ($supplier){
                return jsonResponse(TRUE)->message('Skill deleted successfully.');
            }else{
                return jsonResponse(TRUE)->message('Skill delete failed.');
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
