<?php

namespace App\Http\Services\User;

use App\Http\Repositories\User\UserRepository;
use App\Http\Services\BaseService;
use App\Models\Role\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param User/UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repo = $repository;
    }

    public function getUserList(){
        return $this->repo->getUserList();
    }

    public function getSalesmanList(){
        return $this->repo->getSalesmanList();
    }

    public function getUserData($id){
        if ($id !== NULL){
            $data['user'] = $this->repo->getUserDetails($id);
        }else{
            $data['user'] = [];
        }
        $data['roles'] = Role::all();
        return $data;
    }

    public function checkSlug($slug,$user_id){
        $slug = $this->repo->getSlug($slug,$user_id);
        try {
            if ($slug){
                return jsonResponse(FALSE)->message(__('Unique and valid Slug required'));
            }else{
                return jsonResponse(TRUE)->message(__('Valid Slug'));
            }

        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }
    }

    public function create(array $requestArray) {

        try {
            DB::beginTransaction();
            $requestArray['password'] = bcrypt(123456);
            $requestArray['remember_token'] = md5($requestArray['email'] . uniqid() . randomString(5));
            $user = $this->repo->create($requestArray);
            if ( $user) {
                $this->repo->userPasswordChangeMail($user);
                DB::commit();
                return jsonResponse(true)->message(__("User been created successfully."));
            }
            DB::rollBack();
            return jsonResponse(false)->message(__("User create failed."));
        } catch (\Exception $e) {
            DB::rollBack();
            return jsonResponse(false)->message($e->getMessage());
        }
    }

    public function update(int $id, array $requestArray) {
        try {
            $response = $this->repo->updateModel($id, $requestArray);
            return !$response ? jsonResponse(false)->default() :
                jsonResponse(true)->message(__("User has been updated successfully"));
        } catch (\Exception $e) {
            return jsonResponse(false)->default();
        }
    }

    public function delete($id){
        try {
            $supplier = $this->repo->destroy($id);
            if ($supplier){
                return jsonResponse(TRUE)->message('Supplier deleted successfully.');
            }else{
                return jsonResponse(TRUE)->message('Supplier delete failed.');
            }
        }catch (\Exception $exception){
            return jsonResponse(FALSE)->default();
        }

    }
}
