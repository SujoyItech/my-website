<?php

namespace App\Http\Services;

use App\Http\Repositories\SettingRepository;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingService extends BaseService
{
    /**
     * Instantiate repository
     *
     * @param SettingRepository $repository
     */
    public function __construct(SettingRepository $repository)
    {
        $this->repo = $repository;
    }

    // Your methods for repository


}
