<?php

namespace App\Http\Services\RoleManagementServices;


use App\Http\Repositories\RoleManagementRepositories\RouteRepository;
use App\Http\Services\BaseService;
use App\Models\Role\RoleRoute;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class RouteService extends BaseService
{
    public function __construct(RouteRepository $repository)
    {
        $this->repo = $repository;
    }
    // Your methods for repository

    public function updateRouteList(){
        try {
            $response = $this->repo->updateRouteList();
            if ($response){
                return jsonResponse(TRUE)->message(__("Routing List synced successfully."));
            }else{
                return jsonResponse(FALSE)->message(__("Routing List sync failed."));
            }
        }catch (\Exception $exception){
            return jsonResponse(false)->default();
        }
    }

    public static function generateRouteList(){
        $roles_route = [];
        foreach (RoleRoute::all() as $value){
            $roles_route[$value->url] = $value->name;
        }
        $routes = Route::getRoutes();
        $admin_routes = [];
        foreach ($routes as $route) {
            $middleware = $route->middleware();
            for ($i = 0; $i < count($middleware); $i++) {
                if ($middleware[$i] == 'super_admin_permission') {
                    $admin_routes[] = array(
                        'type' => SUPER_ADMIN,
                        'name' => isset($role_routes[$route->getName()]) ? $role_routes[$route->getName()] : $route->getName(),
                        'url' => $route->getName()
                    );
                }elseif ($middleware[$i] == 'admin_permission') {
                    $admin_routes[] = array(
                        'type' => USER_ROLE_ADMIN,
                        'name' => isset($role_routes[$route->getName()]) ? $role_routes[$route->getName()] : $route->getName(),
                        'url' => $route->getName()
                    );
                }elseif ($middleware[$i] == 'product_manager_permission'){
                    $admin_routes[] = array(
                        'type' => PRODUCT_MANAGER_MODULE,
                        'name' => isset($role_routes[$route->getName()]) ? $role_routes[$route->getName()] : $route->getName(),
                        'url' => $route->getName()
                    );
                }elseif ($middleware[$i] == 'production_manager_permission'){
                    $admin_routes[] = array(
                        'type' => PRODUCTION_MANAGER_MODULE,
                        'name' => isset($role_routes[$route->getName()]) ? $role_routes[$route->getName()] : $route->getName(),
                        'url' => $route->getName()
                    );
                }elseif ($middleware[$i] == 'crm_manager_permission'){
                    $admin_routes[] = array(
                        'type' => CRM_MANAGER_MODULE,
                        'name' => isset($role_routes[$route->getName()]) ? $role_routes[$route->getName()] : $route->getName(),
                        'url' => $route->getName()
                    );
                }elseif ($middleware[$i] == 'website_manager_permission'){
                    $admin_routes[] = array(
                        'type' => WEBSITE_MANAGER_MODULE,
                        'name' => isset($role_routes[$route->getName()]) ? $role_routes[$route->getName()] : $route->getName(),
                        'url' => $route->getName()
                    );
                }
            }
        }
        try {
            Storage::disk('public')->put('routes.json', json_encode($admin_routes));
            return true;
        }catch (\Exception $e){
            return false;
        }
    }
}
