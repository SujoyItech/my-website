<?php
Route::group(['middleware'=>['auth:sanctum', 'verified','admin_permission','role_permission']],function () {
    Route::get('admin-home', 'Web\Admin\DashboardController@index')->name('adminHome');

    /*==============================================Settings===============================================*/
    Route::get('site-settings','Web\Admin\Settings\SettingsController@siteSetting')->name('siteSetting');
    Route::get('logo-settings','Web\Admin\Settings\SettingsController@logoSetting')->name('logoSetting');
    Route::get('social-settings','Web\Admin\Settings\SettingsController@socialSetting')->name('socialSetting');
    Route::post('admin-settings-save','Web\Admin\Settings\SettingsController@adminSettingsSave')->name('adminSettingsSave');

    Route::get('language-settings', 'Settings\LanguageSettingController@languageSettings')->name('languageSettings');
    Route::get('language-add', 'Settings\LanguageSettingController@addLanguage')->name('addLanguage');

    /*==============================================Settings===============================================*/

    Route::get('profile', 'Web\Admin\Profile\ProfileController@index')->name('profile');
    Route::post('update-profile', 'Web\Admin\Profile\ProfileController@updateProfile')->name('updateProfile');
    Route::post('update-password', 'Web\Admin\Profile\ProfileController@updatePassword')->name('updatePassword');

    Route::get('brand','Web\Admin\Personal\BrandController@index')->name('brands');
    Route::post('brand-edit','Web\Admin\Personal\BrandController@edit')->name('editBrand');
    Route::post('brand-save','Web\Admin\Personal\BrandController@store')->name('saveBrand');
    Route::post('brand-slug-check','Web\Admin\Personal\BrandController@brandSlugCheck')->name('brandSlugCheck');
    Route::post('brand-delete','Web\Admin\Personal\BrandController@delete')->name('deleteBrand');

    Route::get('education','Web\Admin\Personal\EducationController@index')->name('education');
    Route::post('education-edit','Web\Admin\Personal\EducationController@edit')->name('editEducation');
    Route::post('education-save','Web\Admin\Personal\EducationController@store')->name('saveEducation');
    Route::post('education-delete','Web\Admin\Personal\EducationController@delete')->name('deleteEducation');

    Route::get('skills','Web\Admin\Personal\SkillController@index')->name('skills');
    Route::post('skill-edit','Web\Admin\Personal\SkillController@edit')->name('editSkill');
    Route::post('skill-save','Web\Admin\Personal\SkillController@store')->name('saveSkill');
    Route::post('skill-delete','Web\Admin\Personal\SkillController@delete')->name('deleteSkill');

});



