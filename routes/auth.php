<?php

Route::get('/', 'Web\Auth\AuthController@login');

Route::get('forget-password', 'Web\Auth\AuthController@forgetPassword')->name('forgetPassword');
Route::post('send-forget-password-mail', 'Web\Auth\AuthController@sendForgetPasswordMail')->name('sendForgetPasswordMail');
Route::get('reset-password/{token}', 'Web\Auth\AuthController@resetPassword')->name('resetPassword');
Route::post('change-password', 'Web\Auth\AuthController@changePassword')->name('changePassword');
