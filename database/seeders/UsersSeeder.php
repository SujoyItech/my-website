<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(['email' => 'sujoy.csesust@gmail.com'],[
                'id' => 1,
                'name' => 'Sujoy Nath',
                'email' => 'sujoy.csesust@gmail.com',
                'password' => Hash::make('Pass.123')
            ]);


    }
}
