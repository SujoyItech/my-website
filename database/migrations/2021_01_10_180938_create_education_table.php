<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
            $table->string('degree');
            $table->string('institution');
            $table->string('academic_year')->nullable();
            $table->decimal('obtain_result')->nullable();
            $table->decimal('out_of_result')->nullable();
            $table->string('icon')->nullable();
            $table->string('certificate')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
