<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->tinyInteger('level')->nullable();
            $table->tinyInteger('experience_category')->nullable();
            $table->tinyInteger('learned_from')->nullable();
            $table->decimal('experience_year')->nullable();
            $table->integer('total_project')->nullable();
            $table->integer('industrial_project')->nullable();
            $table->integer('personal_project')->nullable();
            $table->string('icon')->nullable();
            $table->text('description')->nullable();
            $table->text('tags')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('order')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skills');
    }
}
